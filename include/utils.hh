#pragma once

#include <spdlog/spdlog.h>

namespace pinit {
namespace utils {

void sleep(unsigned long seconds);
void loop_forever();

} // namespace utils
} // namespace pinit
