
#include <iostream>

#include "spdlog/spdlog.h"

#include <utils.hh>

using namespace pinit;

int main(int argc, char **argv) {
  spdlog::set_level(spdlog::level::debug);
  spdlog::info("Hello, world!");
  pinit::utils::loop_forever();
}
