#include <time.h>

#include <utils.hh>

namespace pinit {
namespace utils {

/**
 * @brief Sleep for @seconds.
 *
 * @param seconds
 */
void sleep(unsigned long seconds) {
  struct timespec ts;
  ts.tv_sec = seconds;
  spdlog::trace("sleeping for {} seconds", seconds);
  nanosleep(&ts, NULL);
}

/**
 * @brief Infinitely loop, usually used when we
 * reach an unrecoverable error so that we avoid
 * a kernel panic.
 */
void loop_forever() {
  while (true)
    sleep(1);
}

} // namespace utils
} // namespace pinit
